/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50622
Source Host           : localhost:3306
Source Database       : strutdb

Target Server Type    : MYSQL
Target Server Version : 50622
File Encoding         : 65001

Date: 2018-06-06 12:45:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for parallel_task_detail
-- ----------------------------
DROP TABLE IF EXISTS `parallel_task_detail`;
CREATE TABLE `parallel_task_detail` (
  `task_id` varchar(64) NOT NULL COMMENT '任务ID',
  `task_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `parent_ids` varchar(1000) DEFAULT NULL COMMENT '所有依赖任务ID, 以","作为分隔符',
  `job_id` varchar(64) DEFAULT NULL COMMENT '所属作业ID',
  `job_name` varchar(100) DEFAULT NULL COMMENT '所属作业名称',
  `bean_class` varchar(1000) NOT NULL COMMENT '任务对应的业务处理器(包名+类名)',
  `task_status` enum('N','D') NOT NULL DEFAULT 'N' COMMENT '任务状态值(N-正常, D-废弃)',
  `priority` int(4) unsigned NOT NULL DEFAULT '100' COMMENT '优先级',
  `task_desc` varchar(250) DEFAULT NULL COMMENT '任务简单描述',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '任务创建日期',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '任务更新日期',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='任务信息详情表';