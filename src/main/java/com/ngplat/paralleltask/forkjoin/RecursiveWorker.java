/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.forkjoin
 * @filename: RecursiveWorker.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.forkjoin;

/**
 * @typename: RecursiveWorker
 * @brief: 递归子任务, 真正执行业务的地方
 * @author: KI ZCQ
 * @date: 2018年6月4日 上午10:23:23
 * @version: 1.0.0
 * @since
 * 
 */
public interface RecursiveWorker<T> {

	/**
	 * @Description: 具体的业务逻辑，放在这个方法里面执行，将返回的结果，封装到context内
	 * @param context
	 */
	void work(T context);
}
