/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: PlusTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: PlusTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午3:40:04
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "1", name = "PlusTask")
public class PlusTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = 993241934061932172L;

	/**
	 * 创建一个新的实例 PlusTask.
	 */
	public PlusTask() {
		
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println(String.format("[PlusTask] runWorker, result: %d", (1 + 1)));
	}

}
